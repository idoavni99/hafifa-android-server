FROM node:latest

COPY . .

RUN ["/bin/bash", "-c", "npm install"]

EXPOSE 3000
CMD [ "/bin/bash" ,"-c" , "npm start" ]
