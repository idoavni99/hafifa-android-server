const app = require('express')();
const server = require("http").createServer(app);
const io = require("socket.io")(server);
const { v4: uuid } = require('uuid');
const port = 3000;
const { transports, format, createLogger } = require('winston');

const logger = createLogger({
    format: format.combine(
        format.timestamp(),
        format.json()
    ),
    transports: [
        new transports.Console()
    ],
    exitOnError: false
});

let users = {};

io.on('connection', socket => {
    let userId = ''
    socket.on('disconnecting', () => {
        logger.info(`Socket ${socket.id} Leaving`)
    })

    socket.on('login', (userName, password) => {
        const userInServer = Object.values(users).find(user => user.userName == userName)
        let user = { userName, password, id: 0, reminders: [] };
        let isLoginSucceded = false;

        if (!userInServer) {
            isLoginSucceded = true;
            user.id = uuid();
            users = { ...users, [user.id]: user }
            userId = user.id
        } else if (userInServer.password === password) {
            userId = userInServer.id;
            user.reminders = userInServer.reminders
            user.id = userId;
            isLoginSucceded = true;
        }

        if (isLoginSucceded) {
            socket.join(userId)
            socket.emit("LOGIN_SUCCESS", user)
            logger.info(`${user.userName} has logged in`)
        } else {
            socket.emit("LOGIN_FAILED", false)
            logger.error(`Login Error`)
        }

    })

    socket.on('loginWithId', (loginUserId) => {
        const userInServer = Object.values(users).find(user => user.id == loginUserId)
        if (userInServer) {
            socket.join(loginUserId)
            userId = loginUserId
            logger.info(`${userInServer.userName} has logged in`)
            socket.emit("LOGIN_SUCCESS", userInServer);
        } else {
            logger.info(`Login Error`)
            socket.emit("LOGIN_FAILED", true)
        }
    })

    socket.on('logout', () => {
        socket.leave(userId)
        logger.info(`${userId} has logged out on socket ${socket.id}`)
        userId = 0;
    })

    socket.on('reminders', () => {
        const reminders = users[userId].reminders;
        socket.emit("REMINDERS_RECEIVED", reminders);
        logger.info(`Synced reminders for user ${users[userId].userName} in socket ${socket.id}`)
    })

    socket.on('changeUsername', (newName) => {
        const isNameBlank = !newName.trim()
        if ((Object.values(users).find(user => user.userName == newName) &&
            newName !== users[userId].userName) || isNameBlank) {
            io.to(userId).emit("USERNAME_CHANGE_FAILED", isNameBlank)
            logger.error(`${users[userId].userName} could not be replaced to ${newName}`)
        } else {
            logger.info(`${users[userId].userName} successfully replaced to ${newName}`)
            users[userId].userName = newName;
            socket.emit("USERNAME_CHANGE_SUCCESS", newName);
            socket.to(userId).emit("USERNAME_REPLACED", newName);
        }
    })

    socket.on('deleteReminder', (reminderId) => {
        const user = users[userId];
        user.reminders = user.reminders.filter(reminder => reminder.id !== reminderId);
        socket.to(userId).emit('REMINDER_DELETED', reminderId);

        logger.info(`Reminder ${reminderId} has been deleted by ${users[userId].userName}`)
    })

    socket.on('saveReminder', reminder => {
        const parsedReminder = JSON.parse(reminder)
        const user = users[userId];

        if (!user.reminders.find(r => r.id == parsedReminder.id)) {
            user.reminders.push(parsedReminder);
            isAdded = true;
            logger.info(`Reminder ${parsedReminder.id} has been added by ${users[userId].userName}`)
            socket.to(userId).emit('REMINDER_ADDED', parsedReminder);
        } else {
            user.reminders = user.reminders.map(item => item.id === parsedReminder.id ? parsedReminder : item)
            logger.info(`Reminder ${parsedReminder.id} has been edited by ${users[userId].userName}`)
            socket.to(userId).emit('REMINDER_EDITED', parsedReminder);
        }
    })

    logger.info(`Socket ${socket.id} has connected`)
})

io.on('disconnect', socket => {
    logger.info(`${socket.id} has disconnected`)
})

server.listen(port, () => console.log(`Server started!`))